
package ufc.br

object Questao02 {
  
  trait Metodos {
    def criarLista() : Lista
    def inserirElemento(lista : Lista, valor : Integer)
    def imprimirValoresDaLista(lista : Lista)
    def imprimirValoresDaListaRecursao(lista : Lista, primeiro : Boolean)
    def imprimirValoresDaListaOrdemReversa(lista : Lista, primeiro : Boolean) : Unit
    def verificarSeListaVazia(lista : Lista): Integer
    def buscarDeterminadoElementoDaLista(lista : Lista, elemento : Integer): Lista
    def removerDeterminadoElementoDaLista(lista: Lista, valor : Integer)
    def removerDeterminadoElementoDaListaRecursao(lista: Lista, valor : Integer, primeiro : Boolean = true, anterior : Lista = null) 
    def liberarLista(lista : Lista)
  }
  
  class Lista(valorParam : Integer = null){
    var valor : Integer = valorParam
    var proximo : Lista = null
    override def toString : String = {
      " Valor: "+valor+ "\n"+(if (proximo != null) proximo else "")
    }
  }
  
  class Implementacao extends Metodos{

    override def criarLista() : Lista = {
      new Lista
    }
    
    //OK
    override def inserirElemento(lista : Lista, valor: Integer) : Unit= {      
      var listaTemp = lista
      var anterior : Lista = null
      
      if(listaTemp == null){
        println("A Lista não está iniciada.")
      }else{
        
        if(listaTemp.valor == null){
          listaTemp.valor = valor
          return
        }
        do{
          if(valor >= listaTemp.valor){
            anterior = listaTemp
            if(listaTemp.proximo == null){
              listaTemp.proximo = new Lista(valor)
              return
            }
            listaTemp = listaTemp.proximo
          }else{
            if(anterior == null){
              var valueTemp = listaTemp.valor
              listaTemp.valor = valor
              var novoValor = new Lista(valueTemp)
              novoValor.proximo = listaTemp.proximo
              listaTemp.proximo = novoValor
              listaTemp = null
            }else{
              var novaLista = new Lista(valor)
              anterior.proximo = novaLista
              novaLista.proximo = listaTemp
              return
            }
          }
        }while(listaTemp!=null)
      }
    }
    
    //OK
    override def imprimirValoresDaLista(lista : Lista) = {
      var listaTemp = lista
      
      if(listaTemp == null || listaTemp.valor == null){
        println("A lista está vazia");
      }else{
        do{
          println(listaTemp.valor)
          listaTemp = listaTemp.proximo
        }while(listaTemp!=null)
      }
    }
    //OK
    override def imprimirValoresDaListaRecursao(lista : Lista, primeiro : Boolean = true) : Unit = {
      var listaTemp = lista
      if(listaTemp == null && primeiro){
        println("A lista está vazia")
        return
      }else if(listaTemp != null){
        println(listaTemp.valor)
        listaTemp = listaTemp.proximo
        imprimirValoresDaListaRecursao(listaTemp, false)
      }
    }
    
    //OK
    override def imprimirValoresDaListaOrdemReversa(lista : Lista, primeiro : Boolean = true) : Unit = {
      if((lista == null || lista.valor == null) && primeiro){
         println("A lista está vazia")
         return
      }else if(lista != null){
          this.imprimirValoresDaListaOrdemReversa(lista.proximo, false)
          println(lista.valor)
      }
    }
    
    //OK
    override def verificarSeListaVazia(lista : Lista): Integer = {
      if (lista == null || lista.valor == null) 1 else 0
    }
    
    //OK
    override def buscarDeterminadoElementoDaLista(lista : Lista, elemento : Integer): Lista = {
      if(lista == null || lista.valor == null){
        println("A lista está vazia")
        null
      }else{
        var listaTemp = lista;
        
        do{
          if(listaTemp.valor == elemento)
            return listaTemp

          listaTemp = listaTemp.proximo;   
        }while(listaTemp != null)
        null
      }
    }
    
    //OK
    override def removerDeterminadoElementoDaLista(lista: Lista, valor : Integer) : Unit = {
       if(lista == null || lista.valor == null){
         println("A lista está vazia")
       }else{       
       
         var tempList = lista
         var anterior : Lista = null
         
         do{
           if(tempList.valor == valor){
           
             if(anterior!=null){
               anterior.proximo = tempList.proximo
               tempList = anterior
             }else{
               tempList.valor = tempList.proximo.valor
               tempList.proximo = tempList.proximo.proximo
             }
             return
             
           }else{
             anterior = tempList
             tempList = tempList.proximo
           }
         }while(tempList != null)
       }
    }
    
    //OK
    override def removerDeterminadoElementoDaListaRecursao(lista: Lista, valor : Integer, primeiro : Boolean = true, anterior : Lista = null) = {
        if(lista != null){
        var temp = lista
        if(temp.valor == valor){
          
          if(primeiro){
            if(temp.proximo!=null){
              temp.valor = temp.proximo.valor
              temp.proximo = temp.proximo.proximo
            }else{
              temp.valor = null
            }
          }else{
            if(anterior!=null){
              anterior.proximo = temp.proximo
            }else{
              temp = temp.proximo
            }
          }
        }else{
          removerDeterminadoElementoDaListaRecursao(temp.proximo, valor, false, temp)
        }
      }
    }
    
    //OK
    override def liberarLista(lista : Lista) = {
       if(lista == null || lista.valor == null){
         println("A lista está vazia")
       }else{
         var proximo = lista
         var apagar = proximo
         
         do{
          proximo.valor = null
          apagar = proximo
          proximo = proximo.proximo
          apagar = null
         }while(proximo!=null)
       }
    }
  }
  
    def main (args : Array[String]) : Unit  = {
    
    val impl = new Implementacao
    
    //1 - Criar uma lista vazia;
    val l: Lista = impl.criarLista
    
    //2 - Inserir elemento no início;
    impl.inserirElemento(l, 4)
    impl.inserirElemento(l, 2)
    impl.inserirElemento(l, 1)
    impl.inserirElemento(l, 3)
    
    println("\n3 )")
    //3 - Imprimir os valores armazenados na lista;
    impl.imprimirValoresDaLista(l);
    
    println("\n4 )")
    //4 - Imprimir os valores armazenados na lista usando recursão;
    impl.imprimirValoresDaListaRecursao(l)
    
    println("\n5 )")
    //5 - Imprimir os valores armazenados na lista em ordem reversa;
    impl.imprimirValoresDaListaOrdemReversa(l)
    
    println("\n6 )")
    //6 - Verificar se a lista está vazia
    println(impl.verificarSeListaVazia(l))
    
    println("\n7 )")
    //7 - Recuperar/Buscar um determinado elemento da lista;
    println(impl.buscarDeterminadoElementoDaLista(l, 1))
    
    //8. Remover um determinado elemento da lista;
    impl.removerDeterminadoElementoDaLista(l, 1)
    
    println("\n3 )")
    //3 - Imprimir os valores armazenados na lista;
    impl.imprimirValoresDaLista(l);

    //9. Remover um determinado elemento da lista usando recursão;
    //impl.removerDeterminadoElementoDaListaRecursao(l, 88)
    
    println("\n10 )")
    //10. Liberar a lista;
    impl.liberarLista(l);
    
     println("\n3 )")
    //3 - Imprimir os valores armazenados na lista;
    impl.imprimirValoresDaLista(l);
  }
}