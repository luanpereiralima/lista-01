
package ufc.br

object Questao05 {
  
  trait Metodos {
    def criarLista() : Lista
    def inserirElemento(lista : Lista, valor : Integer)
    def imprimirValoresDaLista(lista : Lista)
    def imprimirValoresDaListaRecursao(lista : Lista, primeiro : Boolean, elemento : No = null)
    def imprimirValoresDaListaOrdemReversa(lista : Lista, primeiro : Boolean, elemento : No = null) : Unit
    def verificarSeListaVazia(lista : Lista): Integer
    def buscarDeterminadoElementoDaLista(lista : Lista, elemento : Integer): No
    def removerDeterminadoElementoDaLista(lista: Lista, valor : Integer) : Unit
    def removerDeterminadoElementoDaListaRecursao(lista: Lista, valor : Integer, primeiro : Boolean,  verificado : No = null) : Unit
    def liberarLista(lista : Lista)
  }
  
  class No(valorParam : Integer = null){
    var valor : Integer = valorParam
    var proximo : No = null
    var anterior : No = null
  }
  
  class Lista{
    var inicio : No = null
    var fim : No = null
    var tamanho : Int = 0
  }
  
  class Implementacao extends Metodos{
    
    //OK
    override def criarLista() : Lista = {
      new Lista
    }
    
    //OK
    override def inserirElemento(lista : Lista, valor: Integer) : Unit= {      
      var listaTemp = lista
      
      if(listaTemp == null){
        println("A Lista não está iniciada.")
      }else{
        var novoElemento = new No(valor)
        if(lista.tamanho == 0){
          novoElemento.proximo = novoElemento
          novoElemento.anterior = novoElemento
          lista.inicio = novoElemento
        }else{
          novoElemento.proximo = lista.fim.proximo;
          novoElemento.anterior = lista.fim
          lista.fim.proximo.anterior = novoElemento
          lista.fim.proximo = novoElemento;
        }
        lista.fim = novoElemento;
        lista.tamanho+=1;  
      }
    }
    
    //OK
    override def imprimirValoresDaLista(lista : Lista) : Unit = {
      var listaTemp = lista.inicio
      
      if(listaTemp == null || lista.tamanho == 0){
        println("A lista está vazia");
      }else{
        do{
          println(listaTemp.valor)
          if(listaTemp == lista.fim)
            return
          listaTemp = listaTemp.proximo
        }while(listaTemp!=null)
      }
    }
    
    //OK
    override def imprimirValoresDaListaRecursao(lista : Lista, primeiro : Boolean = true, elemento : No = null) : Unit = {
      
      var noTemp = lista.inicio
      
      if(elemento != null)
        noTemp = elemento
            
      if((lista == null  || lista.tamanho == 0) && primeiro){
        println("A lista está vazia")
        return
      }else if(noTemp != null){
        println(noTemp.valor)
          if(noTemp == lista.fim)
            return
        noTemp = noTemp.proximo
        imprimirValoresDaListaRecursao(lista, false, noTemp)
      }
    }
    
    //OK
    override def imprimirValoresDaListaOrdemReversa(lista : Lista, primeiro : Boolean = true, elemento : No = null) : Unit = {
      
      var noTemp = lista.inicio
      
      if(elemento != null)
        noTemp = elemento
      
      if((lista == null || lista.tamanho == 0) && primeiro){
         println("A lista está vazia")
         return
      }else if(noTemp != null){
          if(noTemp == lista.fim){
            println(noTemp.valor)
            return
          }
          this.imprimirValoresDaListaOrdemReversa(lista, false, noTemp.proximo)
          println(noTemp.valor)
      }
    }
    
    //OK
    override def verificarSeListaVazia(lista : Lista): Integer = {
      if (lista == null || lista.tamanho == 0) 1 else 0
    }
    
    //OK
    override def buscarDeterminadoElementoDaLista(lista : Lista, elemento : Integer): No = {
      if(lista == null || lista.tamanho == 0){
        println("A lista está vazia")
        null
      }else{
        var listaTemp = lista.inicio;
        
        do{
          if(listaTemp.valor == elemento)
            return listaTemp
            
          if(listaTemp == lista.fim)
             return null

          listaTemp = listaTemp.proximo;   
        }while(listaTemp != null)
        null
      }
      null
    }
    
    //OK
    override def removerDeterminadoElementoDaLista(lista: Lista, valor : Integer) : Unit = {
       if(lista == null || lista.tamanho == 0){
         println("A lista está vazia")
       }else{       
       
         if(lista.inicio == lista.fim){
           lista.inicio = null
           lista.fim = null
           lista.tamanho = 0
         }else{
           var tempLista : No = lista.inicio
           
           do{
             if(tempLista.valor == valor){
               if(tempLista.anterior == lista.inicio){
                 tempLista.proximo.anterior = tempLista.anterior
                 tempLista.proximo = tempLista.proximo
               }else{
                 tempLista.proximo.anterior = tempLista
                 tempLista = tempLista.proximo
                 lista.fim.proximo = tempLista
                 lista.inicio = tempLista
               }
               lista.tamanho-=1
               return 
             }else{
               tempLista = tempLista.proximo
             }
           }while(tempLista!=lista.fim)
         }
       }
    }
    
    //OK
    override def removerDeterminadoElementoDaListaRecursao(lista: Lista, valor : Integer, primeiro : Boolean = true,  verificado : No = null) : Unit = {
       if(lista == null || lista.tamanho == 0){
         println("A lista está vazia")
         return 
       }else{       
         
         var verificadoTemp : No = verificado
         
         if(primeiro){
           verificadoTemp = lista.inicio
         }
         
         if(verificadoTemp==null && !primeiro)
           return
         else{
           if(lista.inicio == lista.fim){
             lista.inicio = null
             lista.fim = null
             lista.tamanho = 0
           }else{
               if(verificadoTemp.valor == valor){
                 if(verificadoTemp.anterior == lista.inicio){
                   verificadoTemp.proximo.anterior = verificadoTemp.anterior
                   verificadoTemp.anterior.proximo = verificadoTemp.proximo
                 }else{
                   verificadoTemp.proximo.anterior = verificadoTemp
                   verificadoTemp = verificadoTemp.proximo
                   lista.fim.proximo = verificadoTemp
                   lista.inicio = verificadoTemp
                 }
                 lista.tamanho-=1
                 return 
               }else{
                 if(verificadoTemp==lista.fim){
                   return
                 }
                 verificadoTemp = verificadoTemp.proximo
                 
               }
             removerDeterminadoElementoDaListaRecursao(lista, valor, false, verificadoTemp)
           }
         }
       }
    }
    
    //OK
    override def liberarLista(lista : Lista) : Unit = {
       if(lista == null || lista.tamanho == 0){
         println("A lista está vazia")
       }else{
         lista.inicio = null
         lista.fim = null
         lista.tamanho = 0
       }
    }
  }
  
    def main (args : Array[String]) : Unit  = {
    
    val impl = new Implementacao
    
    //1 - Criar uma lista vazia;
    val l: Lista = impl.criarLista
    
    //2 - Inserir elemento no início;
    impl.inserirElemento(l, 1)
    impl.inserirElemento(l, 2)
    impl.inserirElemento(l, 3)
    impl.inserirElemento(l, 4)
    
    println("\n3 )")
    //3 - Imprimir os valores armazenados na lista;
    impl.imprimirValoresDaLista(l);
    
    println("\n4 )")
    //4 - Imprimir os valores armazenados na lista usando recursão;
    impl.imprimirValoresDaListaRecursao(l)
    
     println("\n4 )")
    //4 - Imprimir os valores armazenados na lista usando recursão;
    impl.imprimirValoresDaListaRecursao(l)
    
    println("\n5 )")
    //5 - Imprimir os valores armazenados na lista em ordem reversa;
    impl.imprimirValoresDaListaOrdemReversa(l)
    
    println("\n6 )")
    //6 - Verificar se a lista está vazia
    println(impl.verificarSeListaVazia(l))
    
    println("\n7 )")
    //7 - Recuperar/Buscar um determinado elemento da lista;
    println(impl.buscarDeterminadoElementoDaLista(l, 1).valor)
    
     //8. Remover um determinado elemento da lista;
    impl.removerDeterminadoElementoDaListaRecursao(l, 1)
        println("\n3 removendo 1)")

    //3 - Imprimir os valores armazenados na lista;
    impl.imprimirValoresDaLista(l);
     
    impl.removerDeterminadoElementoDaListaRecursao(l, 2)
         println("\n3 removendo 2)")
    //3 - Imprimir os valores armazenados na lista;
    impl.imprimirValoresDaLista(l);
    
    impl.removerDeterminadoElementoDaListaRecursao(l, 0)
          println("\n3 ) removendo 0")
    //3 - Imprimir os valores armazenados na lista;
    impl.imprimirValoresDaLista(l);
      
    impl.removerDeterminadoElementoDaListaRecursao(l, 3)
           println("\n3 removendo 3)")
    //3 - Imprimir os valores armazenados na lista;
    impl.imprimirValoresDaLista(l);
       
    impl.removerDeterminadoElementoDaListaRecursao(l, 4)
            println("\n3 removendo 4)")
    //3 - Imprimir os valores armazenados na lista;
    impl.imprimirValoresDaLista(l);
    
    impl.removerDeterminadoElementoDaListaRecursao(l, 3)
      println("\n3 removendo 3)")
    //3 - Imprimir os valores armazenados na lista;
    impl.imprimirValoresDaLista(l);
    
    //9. Remover um determinado elemento da lista usando recursão;
    //impl.removerDeterminadoElementoDaListaRecursao(l, 88)
    
    println("\n10 )")
    //10. Liberar a lista;
    impl.liberarLista(l);
    
       println("\n3 )")
    //3 - Imprimir os valores armazenados na lista;
    impl.imprimirValoresDaLista(l);
  }
}